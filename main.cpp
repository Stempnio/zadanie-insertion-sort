#include <iostream>
#include <stdlib.h>

using namespace std;

class Sort {

    int* array;
    int n; // ilosc elementow tablicy

public:
    Sort() {
        cout << "Podaj ilosc testow ktore chcesz przeprowadzic?" << endl;
        int numberOfTests;
        cin >> numberOfTests;
        if(numberOfTests <= 0) {
            cout << "Podano niepoprawna wartosc!!" << endl;
            exit(1);
        }
        for(int i=0; i<numberOfTests; i++) {
            createNewArray();
            insertionSort();
            display();
            if(!check()) {
                cout << "Program dziala niepoprawnie!!" << endl;
                delete array;
                return;
            }
            delete array;
        }

        cout << "Wykonano pomyslnie " << numberOfTests << " testow. Wynik poprawny, tablice sa posortowane." << endl;

    }

    void insertionSort() {
        int element, i;
        for(int j=1; j<n; j++) {
            element = array[j];
            i = j-1;
            while(i >= 0 && array[i] > element) {
                array[i+1] = array[i];
                i = i-1;
            }
            array[i+1] = element;
        }
    }

    bool check() { // sprawdza czy tablica jest posortowana
        bool poprawne = true; // czy zachowany jest porzadek
        for(int i=0; i<n-1; i++) {
            if(array[i] > array[i+1]) {
                poprawne = false;
                break;
            }
        }
        return poprawne;
    }

    void createNewArray() { // tworzy nowa tablice z losowymi elementami
        n = rand() % 100; // losowa wielkosc tablicy
        array = new int[n];
        //losowanie tablicy n elementowej
        for(int i=0; i<n; i++)
            array[i] = rand() % 10000;
    }

    void display() { // wyswietla tablice na ekran
        for(int i=0; i<n; i++) {
            cout << array[i] << " ";
        }
        cout << endl;
        for(int i=0; i<15; i++)
            cout << "-";
        cout << endl;
    }


};

int main() {

    Sort* sort = new Sort();

    delete sort;

    return 0;
}


